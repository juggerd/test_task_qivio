var app = angular.module('app', []);

app.controller('CommentListController', function($scope, $http) {
  $http.get('/api/comment/list').success(function(data) {
    //console.log(data);
    $scope.comments = data;
  });
  $scope.comment = {};
  $scope.add = function(comment) {
    var data = JSON.stringify({
      autor: comment.autor,
      comment: comment.text
    });
    //console.log(data);
    $http.post('/api/comment/add', data)
      .success(function (data, status, headers, config) {
        //console.log(data);
        $scope.comments.push({
          autor: comment.autor,
          date: data.date,
          comment: comment.text
        });
        $scope.reset(comment);
        $('#modalSuccess').modal('show');
      })
      .error(function (data, status, headers, config) {
        console.log(data);
      });
  };
  $scope.reset = function(comment) {
    angular.copy({}, comment);
  }
});
