'use strict';

const bodyParser = require('body-parser');

let express = require('express')();
let config = require(__basedir + '/config');

express.disable('x-powered-by');
express.use(bodyParser.urlencoded({
	extended: true
}));
express.use(bodyParser.json());
express.use('/', require('express').static('public'));
express.use('/', require(__basedir + '/lib/router'));
express.use('*', function(req, res) {
	res.status(404).json({ message: 'Service is temporarily unavailable, please try later' });
});

module.exports = express;
