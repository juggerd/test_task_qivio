'use strict';

const express = require('express');
const co = require('co');
const _ = require('lodash');
const moment = require('moment');

let config = require(__basedir + '/config');
let logger = require(__basedir + '/lib/logger');
let fn = require(__basedir + '/lib/fn');

let router = express.Router();

router.get('/', function(req, res) {
	res.render('index');
});

router.get('/api/comment/list', function(req, res) {
	co(function*(){
		let data = yield fn.readFile(__basedir + '/data/comment.json');
		return data ? JSON.parse(data) : [];
	}).then(function(data){
		res.json(data);
	}).catch(function(err){
		logger.error(err);
		res.json({
			status: 'error',
		});
	});
});

router.post('/api/comment/add', function(req, res) {
	co(function*(){
		let data = yield fn.readFile(__basedir + '/data/comment.json');
		data = data ? JSON.parse(data) : [];
		let comment = {
			autor: req.body.autor,
			date: moment().valueOf(),
			comment: req.body.comment
		};
		data.push(comment);
		yield fn.writeFile(__basedir + '/data/comment.json', JSON.stringify(data));
		return comment;
	}).then(function(comment){
		res.json({
			status: 'success',
			date: comment.date
		});
	}).catch(function(err){
		logger.error(err);
		res.json({ status: 'error' });
	});
});

module.exports = router;
