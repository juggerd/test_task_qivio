INSTALL
---
cd /tmp
git clone https://bitbucket.org/juggerd/test_task_qivio.git
cd /tmp/test_task_qivio
npm i pm2 -g
pm2 start pm2.json

RESULT
---
http://127.0.0.1:8080
