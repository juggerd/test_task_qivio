'use strict';

global.__basedir = __dirname;

let config = require(__basedir + '/config');
let logger = require(__basedir + '/lib/logger');
let express = require(__basedir + '/lib/express');

process.on('uncaughtException', function(err) {
	logger.error(err)
});

process.on('ReferenceError', function(err) {
	logger.error(err)
});

express.listen(config.http.port, config.http.host, function() {
	logger.info('webServer started: '+ config.http.host + ':' + config.http.port);
});
