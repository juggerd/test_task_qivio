let config = {};

config.http = {
  host: '127.0.0.1',
  port: 8080
};

module.exports = config;
